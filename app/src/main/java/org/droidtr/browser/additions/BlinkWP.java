package org.droidtr.browser.additions;

import android.app.*;
import android.content.*;
import android.os.*;
import org.droidtr.browser.*;

public class BlinkWP extends Activity{
	public void onCreate(Bundle b){
		super.onCreate(b);
		try{ 
			SharedPreferences.Editor se = (getSharedPreferences(getPackageName()+".settings",Context.MODE_PRIVATE)).edit();
			se.putString("url","http://blinkcursor.org/");
			se.commit();
			startActivity(new Intent(this,MainActivity.class));
			finish(); 
		} catch(Exception e){}
	}
}
